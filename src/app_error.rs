use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use serde_json::json;
use std::fmt;

#[derive(Debug)]
pub struct AppError {
    pub status_code: u16,
    pub message: String,
}

impl AppError {
    pub fn new(status_code: u16, message: String) -> AppError {
        AppError {
            status_code,
            message,
        }
    }
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(self.message.as_str())
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        let body = Json(json!({
            "error": self.message,
        }));

        (StatusCode::from_u16(self.status_code).unwrap(), body).into_response()
    }
}

// impl From<WorkerError> for AppError {
//     fn from(error: WorkerError) -> AppError {
//         AppError::new(500, format!("worker::Error: {}", error))
//     }
// }

/*
impl From<DieselError> for AppError {
    fn from(error: DieselError) -> AppError {
        match error {
            DieselError::DatabaseError(_, err) => AppError::new(409, err.message().to_string()),
            DieselError::NotFound => AppError::new(404, "Record not found".to_string()),
            err => AppError::new(500, format!("Diesel error: {}", err)),
        }
    }
}
*/
/*
impl ResponseError for AppError {
    fn error_response(&self) -> HttpResponse {
        let status_code = match StatusCode::from_u16(self.status_code) {
            Ok(status_code) => status_code,
            Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
        };

        let message = match status_code.as_u16() < 500 {
            true => self.message.clone(),
            false => {
                println!("{}", self.message);
                "Internal server error".to_string()
            },
        };

        HttpResponse::build(status_code)
            .json(json!({ "message": message }))
    }
}
*/
/* ---------------
use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use diesel::result::Error as DieselError;
use serde::Deserialize;
use serde_json::json;
use std::fmt;

#[derive(Debug, Deserialize)]
pub struct AppError {
    pub status_code: u16,
    pub message: String,
}
*/
