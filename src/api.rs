use crate::{app_error::AppError, db};
use crate::user::User;
use crate::db::Db;

//use crate::{app_error, user};
use axum::{
    extract::State, http::{header::HeaderMap, HeaderValue}, response::{IntoResponse, Json}
};

use cookie::{time::Duration, Cookie, CookieJar, Key, SameSite};
//use headers::Cookie as CookieHeader;
use serde::Deserialize;
//use serde_json::{json, Value};


pub async fn me(headers: HeaderMap) -> Result<Json<User>, AppError> {
    let user = session_user(&headers);
    println!("@@@me: user={:?}", user);

    match user {
        Some(user) => Ok(Json(user)),
        None => Err(AppError::new(401, "No session cookie".to_string())),
    }

    // let mut headers = HeaderMap::new();
    // headers.insert("Set-Cookie", HeaderValue::from_static("kokeilu2=foo2"));
    // //Json<Value> {
    // (headers, Json(json!({ "error": "unimplemented!" })))
}

#[derive(Deserialize, Debug)]
pub struct LoginReq {
    username: String,
    password: String,
}
pub async fn login(State(db): State<Db>, Json(login): Json<LoginReq>) -> impl IntoResponse {
    println!("Login attempt: {}/{}", login.username, login.password);

    if login.username.is_empty() || login.password.is_empty() {
        return Err(AppError::new(400, "Failed validation".to_string()));
    }

    let user = db::db_get_user(&db, &login.username).await?;
    if user.is_none() {
        return Err(AppError::new(401, "User not found".to_string()));
    }
    let user  = user.unwrap();

    if user.password != Some(login.password) {
        return Err(AppError::new(401, "Wrong password".to_string()));
    }

    let user = User {
        password: None,
        ..user
    };

    let cookie_str = create_session_cookie_string(&user);

    let mut headers = HeaderMap::new();
    headers.insert("Set-Cookie", HeaderValue::from_str(&cookie_str).unwrap());
    // (headers, Json(json!({ "error": "unimplemented!" })))
    Ok((headers, Json(user)))
}

pub async fn register(State(db): State<Db>, Json(register): Json<LoginReq>) -> impl IntoResponse {
    println!("Register: {}/{}", register.username, register.password);

    if register.username.is_empty() || register.password.is_empty() {
        return Err(AppError::new(400, "Failed validation".to_string()));
    }

    let user = User {
        username: register.username,
        password: Some(register.password),
        status: Some("Great!".to_string()),
        //..Default::default()
    };

    db::db_set_user(&db, &user).await?;

    let user = User {
        password: None,
        ..user
    };

    let cookie_str = create_session_cookie_string(&user);

    let mut headers = HeaderMap::new();
    headers.insert("Set-Cookie", HeaderValue::from_str(&cookie_str).unwrap());

    Ok((headers, Json(user)))
}

pub async fn logout() -> impl IntoResponse {
    println!("logout");

    let mut headers = HeaderMap::new();
    headers.insert(
        "Set-Cookie",
        HeaderValue::from_str("SESSION=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT").unwrap(),
    );

    (headers, ())
}

fn session_user(headers: &HeaderMap) -> Option<User> {
    if let Some(c) = headers.get("cookie") {
        if let Ok(cookie_str) = std::str::from_utf8(c.as_bytes()) {
            println!("cookie string=`{cookie_str}`");
            let cookie_str = cookie_str.to_string();

            let key = Key::from(crate::SESSION_KEY.get().unwrap());
            let mut jar = CookieJar::new();
            if let Ok(cookie) = Cookie::parse(cookie_str) {
                jar.add(cookie);
                if let Some(decoded_cookie) = jar.private(&key).get("SESSION") {
                    let user_json = &decoded_cookie.to_string()[8..];
                    return match serde_json::from_str::<User>(user_json) {
                        Ok(user) => Some(user),
                        Err(err) => {
                            println!("Error parsing user from cookie: {}", err);
                            None
                        }
                    };
                }
            }
        }
    }

    None
}

fn create_session_cookie_string(user: &User) -> String {
    let user_string = serde_json::to_string(user).unwrap_or_default();
    let cookie = Cookie::build(("SESSION", user_string))
        .path("/")
        .same_site(SameSite::None)
        .secure(true)
        .http_only(true)
        .max_age(Duration::days(1));

    let key = Key::from(crate::SESSION_KEY.get().unwrap());
    let mut jar = CookieJar::new();
    jar.private_mut(&key).add(cookie);
    let cookie = jar.get("SESSION").unwrap();
    cookie.to_string()
}
