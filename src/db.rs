use std::sync::Arc;
use tokio::sync::RwLock;
use std::collections::HashMap;

use crate::app_error::AppError;
use crate::user::User;

pub type Db = Arc<RwLock<HashMap<String, User>>>;

pub async fn db_init() -> Db {
    Arc::new(RwLock::new(HashMap::new()))
}

pub async fn db_get_user(db : &Db, username: &str) -> Result<Option<User>, AppError> {

    let user = db.read().await.get(username).cloned();
    Ok(user)
}

pub async fn db_set_user(db : &Db, user: &User) -> Result<Option<User>, AppError> {

    let mut db = db.write().await;
    db.insert(user.username.clone(), user.clone());

    Ok(None)
}
