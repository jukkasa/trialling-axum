use axum::{
    response::Html,
    routing::{get, post},
    Router,
};
use base64ct::{Base64, Encoding};
use dotenv::dotenv;
use once_cell::sync::OnceCell;
use std::env;
use tracing::Level;
use tower_http::{
    services::{ServeDir, ServeFile},
    trace::{self, TraceLayer},
};
mod user;
//use crate::user::User;
mod api;
mod app_error;
mod db;

// Global environmental/secret values
pub static SESSION_KEY: OnceCell<[u8; 64]> = OnceCell::new();

#[tokio::main]
async fn main() {
    dotenv().ok();
    // Set the RUST_LOG, if it hasn't been explicitly defined
    // if std::env::var_os("RUST_LOG").is_none() {
    //     std::env::set_var("RUST_LOG", "trialling_axum=debug,tower_http=debug")
    // }
    tracing_subscriber::fmt()
        .with_target(false)
        .compact()
        .init();

    let host = env::var("HOST").unwrap_or_else(|_| "::".to_string());
    let port = env::var("PORT").unwrap_or_else(|_| "3000".to_string());

    // Get cookie session secret key (must be 64 bytes encoded with base64)
    let session_key = env::var("SESSION_KEY")
        .unwrap_or_else(|_| "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==".to_string());
    // Set global constants (once_cell)
    let mut key: [u8; 64] = [0; 64];
    let _ = Base64::decode(session_key, &mut key).unwrap();
    let _ = SESSION_KEY.set(key);

    let db = db::db_init().await;

    let serve_dir = ServeDir::new("static").not_found_service(ServeFile::new("static/404.html"));

    let app = Router::new()
        .route("/health", get(empty))
        .route("/alive", get(alive))
        .route("/me", get(api::me))
        .route("/login", post(api::login))
        .route("/register", post(api::register))
        .route("/logout", post(api::logout))
        .fallback_service(serve_dir)
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
                .on_response(trace::DefaultOnResponse::new().level(Level::INFO)),
        )
        .with_state(db);

    // run it
    tracing::info!("listening on http://{host}:{port}");
    let listener = tokio::net::TcpListener::bind(&format!("{}:{}", host, port)).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}

async fn alive() -> Html<&'static str> {
    Html(
        r#"
        <!doctype html>
        <html>
            <head></head>
            <body>
                <h1>I am here!</h1>
            </body>
        </html>
        "#,
    )
}

// Empty response
async fn empty() {}
