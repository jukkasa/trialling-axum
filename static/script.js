
const apiUrl = '.';

const loginForm = document.querySelector("#login");
loginForm.addEventListener("submit", function (e) {
    submitLoginOrRegister(e, this, '/login');
});

const registerForm = document.querySelector("#register");
registerForm.addEventListener("submit", function (e) {
    submitLoginOrRegister(e, this, '/register');
});

const logoutForm = document.querySelector("#logout");
logoutForm.addEventListener("submit", function (e) {
    submitLogout(e);
});

get_me();

async function submitLoginOrRegister(e, form, apiPath) {
    e.preventDefault();
    const jsonFormData = buildJsonFormData(form);
    const resp = await fetch(`${apiUrl}${apiPath}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        credentials: "include",
        body: JSON.stringify(jsonFormData),
    });
    if (resp.ok) {
        await get_me();
    } else {
        let error = await resp.text();
        setMessage(error);
    }
}

async function submitLogout(e) {
    e.preventDefault();
    const resp = await fetch(`${apiUrl}/logout`, {
        method: 'POST',
        credentials: "include",
    });
    await get_me();
}

function buildJsonFormData(form) {
    const jsonFormData = {};
    for (const pair of new FormData(form)) {
        jsonFormData[pair[0]] = pair[1];
    }
    return jsonFormData;
}

function setUserInfo(user) {
    const userinfoEl = document.querySelector('#user');
    if (user?.username) {
        let html = user.username;
        if (user.status) {
            html += `<br>status: ${user.status}`;
        }
        userinfoEl.innerHTML = html;
    } else {
        userinfoEl.innerHTML = "Not logged in";
    }
}

function setMessage(text) {
    const output = document.querySelector("output");
    output.innerText = text;
}

async function get_me() {
    const resp = await fetch(`${apiUrl}/me`, {
        method: 'GET',
        credentials: "include",
    });
    if (resp.ok) {
        const user = await resp.json();
        setUserInfo(user);
        setMessage("");
    } else {
        setUserInfo(null);
        const text = await resp.text();
        setMessage(text);
    }
}
